DwarfExplorer Qt plugin for DFHack
=====================

Browse Dwarf Fortress internal structures graphically!

Like gui/gm-editor with steroids

How to build
------------

Copy/link/clone this directory as a subdirectory in dfhack's `plugins`
directory.  Add the subdirectory (e.g. a `add_subdirectory(dwarfexplorer)` line) in
`plugins/CMakeLists.custom.txt`. Then follow DFHack instructions for building
plugins.

