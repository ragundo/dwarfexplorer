#include "df_model.h"

using namespace rdf;


QString DF_Model::data_from_Type(NodeBase* p_node) const
{
    auto type = p_node->m_used_type;

    if (p_node->m_df_type == rdf::DF_Type::Void)
        return type;

    if (p_node->m_df_type == rdf::DF_Type::Stl_string)
        type = "std::string";

    if (p_node->m_node_type == NodeType::NodeVector)
    {
        NodeVector* node_vector = dynamic_cast<NodeVector*>(p_node);
        auto addornements = node_vector->m_addornements;
        auto qaddornements = QString::fromStdString(addornements.substr(1,100));
        type.append(qaddornements);
    }
    return type;
}
