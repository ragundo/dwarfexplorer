#include <algorithm>
#include <QPixmap>
#include <QListIterator>
#include <QDebug>
#include "df_model.h"
#include "node.h"

using namespace rdf;

extern void fill_node(uint64_t p_df_structure, rdf::Node* p_node_parent);
extern std::array<std::array<std::string, 3>, 32>& get_bitfield_bits(DF_Type);



void DF_Model::set_root(NodeBase* p_node)
{
    beginResetModel();
    m_rootNode = p_node;
    endResetModel();
}


QModelIndex DF_Model::index(int p_row, int p_column, const QModelIndex& p_parent) const
{
    if (!m_rootNode || p_row < 0 || p_column < 0)
        return QModelIndex();
    NodeBase* parentNode = nodeFromIndex(p_parent);
    if (parentNode->m_node_type == NodeType::NodeSimple)
        return QModelIndex();
    NodeBase* childNode = (static_cast<Node*>(parentNode))->m_children.at(p_row);

    return createIndex(p_row, p_column, childNode);
}

QModelIndex DF_Model::parent(const QModelIndex& p_child_index) const
{
    NodeBase* node = nodeFromIndex(p_child_index);
    if (!node)
        return QModelIndex();
    NodeBase* parentNode = node->m_parent;
    if (!parentNode)
        return QModelIndex();
    NodeBase* grandparentNode = parentNode->m_parent;
    if (!grandparentNode)
        return QModelIndex();
    if (grandparentNode->m_node_type == NodeType::NodeSimple)
        return QModelIndex();
    Node* l_gp_node = static_cast<Node*>(grandparentNode);
    int row = l_gp_node->m_children.indexOf(parentNode);
    return createIndex(row, 0, parentNode);
}

int DF_Model::rowCount(const QModelIndex& p_parent) const
{
    qDebug() << "rowCount";
    if (p_parent.column() > 0)
        return 0;
    NodeBase* parentNode = nodeFromIndex(p_parent);
    if (!parentNode)
        return 0;
    if (parentNode->m_node_type == NodeType::NodeSimple)
        return 0;
    auto l_parent = static_cast<Node*>(parentNode);
    return l_parent->m_children.count();
}


NodeBase* DF_Model::nodeFromIndex(const QModelIndex& p_index) const
{
    if (p_index.isValid())
    {
        return static_cast<NodeBase*>(p_index.internalPointer());
    }
    else
    {
        return m_rootNode;
    }
}

QString data_from_Name(const NodeBase* p_node)
{
    return p_node->m_field_name;
}



QString DF_Model::data_from_Address(NodeBase* p_node) const
{
    NodeBase* l_base   = dynamic_cast<NodeBase*>(p_node);
    uint64_t l_address = l_base->m_address;
    if (l_address == 0)
        return "N/A";
    std::string l_address_hex = to_hex(l_address);
    return QString::fromStdString(l_address_hex);
}

QString data_from_Comment(NodeBase* p_node)
{
    NodeBase* l_base = dynamic_cast<NodeBase*>(p_node);
    return l_base->m_comment;
}

QVariant DF_Model::data(const QModelIndex& p_index, int p_role) const
{
    NodeBase* node = nodeFromIndex(p_index);

    if ((p_index.column() == 0) && (p_role == Qt::DecorationRole))
    {
        if ((node->m_rdf_type == rdf::RDF_Type::uint8_t)
                || (node->m_rdf_type == rdf::RDF_Type::uint16_t)
                || (node->m_rdf_type == rdf::RDF_Type::uint32_t)
                || (node->m_rdf_type == rdf::RDF_Type::uint64_t)
                || (node->m_rdf_type == rdf::RDF_Type::int8_t)
                || (node->m_rdf_type == rdf::RDF_Type::int16_t)
                || (node->m_rdf_type == rdf::RDF_Type::int32_t)
                || (node->m_rdf_type == rdf::RDF_Type::int64_t)
                || (node->m_rdf_type == rdf::RDF_Type::Bool))
            return QPixmap(":/circle.png");

        if ((node->m_rdf_type == rdf::RDF_Type::Stl_string) ||
                (node->m_rdf_type == rdf::RDF_Type::Static_string) ||
                (node->m_rdf_type == rdf::RDF_Type::Ptr_string))
            return QPixmap(":/t2_small.png");

        if ((node->m_rdf_type == rdf::RDF_Type::Compound))
            return QPixmap(":/group.png");

        if (node->m_rdf_type == rdf::RDF_Type::Pointer)
            return QPixmap(":/cube.png");

        if (node->m_rdf_type == rdf::RDF_Type::Vector)
            return QPixmap(":/folder.png");

        if ((node->m_rdf_type == rdf::RDF_Type::Enum))
            return QPixmap(":/square.png");

        if ((node->m_rdf_type == rdf::RDF_Type::Bitfield))
            return QPixmap(":/stripes.png");
//        if (node->m_type == NodeType::NodeRange)
//            return QPixmap(":/folder.png");
    }

    if (p_role != Qt::DisplayRole)
        return QVariant();

    if (!node)
        return QVariant();

    switch (p_index.column())
    {
        case 0 :
            return data_from_Name(node);      // Name
        case 1 :
            return data_from_Structure(node); // Structure type
        case 2 :
            return data_from_Type(node);      // Type
        case 3 :
            return data_from_Value(node);     // Value
        case 4 :
            return data_from_Address(node);   // Address
        case 5 :
            return data_from_Comment(node);   // Comment
        default:
            return QVariant();
    }
    return QVariant();
}

QVariant DF_Model::headerData(int p_section, Qt::Orientation p_orientation, int p_role) const
{
    if (p_orientation == Qt::Horizontal && p_role == Qt::DisplayRole)
    {
        switch (p_section)
        {
            case 0 :
                return tr("Name");
            case 1 :
                return tr("Structure");
            case 2 :
                return tr("Type");
            case 3 :
                return tr("Value");
            case 4 :
                return tr("Address");
            case 5 :
                return tr("Comment");
            default:
                return QVariant();
        }
    }
    return QVariant();
}

int DF_Model::columnCount(const QModelIndex& /*p_parent*/) const
{
    return 6;
}

bool DF_Model::removeRows(int p_row, int p_count, const QModelIndex& /*p_parent*/)
{
    return false;
}

bool DF_Model::insertColumns(int p_column, int p_count, const QModelIndex& /*p_parent*/)
{
    return false;
}

bool DF_Model::removeColumns(int p_column, int p_count, const QModelIndex& /*p_parent*/)
{
    return false;
}

void DF_Model::update_node_path(NodeBase* p_source, NodeBase* p_dest)
{
//    QListIterator<NodeBase*> l_iterator(p_source->m_path);
//    while(l_iterator.hasNext())
//        p_dest->m_path.append(l_iterator.next());
}

bool DF_Model::insertRowsDFPointer(const QModelIndex& p_parent, int p_num_rows)
{
//    NodeBase* l_node_base = nodeFromIndex(p_parent);
//    Node* l_node          = dynamic_cast<Node*>(l_node_base);
//    if (l_node->m_children.size() > 0)
//        return false;

//    l_node->update();
//    beginInsertRows(p_parent, 0, p_num_rows);
//    auto l_rdf_object = l_node->get_rdf_object();
//    (*(l_node->m_node_filler))(l_rdf_object, l_node);
//    endInsertRows();
//    return true;
}



bool DF_Model::insertRowsBitfield(const QModelIndex& p_parent)
{
    auto bitfield_node = dynamic_cast<Node*>(nodeFromIndex(p_parent));
    if (bitfield_node->m_children.size() > 0)
        return false;

    auto pointer_bitfield = reinterpret_cast<int32_t*>(bitfield_node->m_address);
    auto bitfield_value = *pointer_bitfield;
    auto bitfield_data = get_bitfield_bits(bitfield_node->m_df_type);
    beginInsertRows(p_parent, 0, bitfield_data.size());
    unsigned int mask = 1;
    for (unsigned int i = 0; i < bitfield_data.size() ; i++)
    {
        QString field_name = QString::fromStdString(bitfield_data[i][1]);
        if ((bitfield_value & mask) || (field_name.length() > 0))
        {
            auto* n_pve          = new NodeBitfieldEntry();
            n_pve->m_field_name  = field_name;
            if (field_name.length() == 0)
            {
                if (i < 10)
                    n_pve->m_field_name = "[0" + QString::fromStdString(std::to_string(i)) + "]";
                else
                    n_pve->m_field_name = "[" + QString::fromStdString(std::to_string(i)) + "]";
            }
            else
            {
                if (i < 10)
                    n_pve->m_field_name = "[0" + QString::fromStdString(std::to_string(i)) + "] " +  field_name;
                else
                    n_pve->m_field_name = "[" + QString::fromStdString(std::to_string(i)) + "] " +  field_name;
            }
            n_pve->m_rdf_type    = rdf::RDF_Type::Bool;
            n_pve->m_df_type     = rdf::DF_Type::Bool;
            n_pve->m_parent      = bitfield_node;
            n_pve->m_index       = i;
            n_pve->m_used_type   = "bool";
            n_pve->m_comment     = QString::fromStdString(bitfield_data[i][2]);
            n_pve->m_value       = bitfield_value & mask;
            n_pve->m_address     = bitfield_node->m_address;
            ;
            bitfield_node->m_children.append(n_pve);
        }

        mask = mask << 1;

//        // Update this node path
//        update_node_path(l_bitfield_node, n_pve);
    }
    endInsertRows();
    return true;
}


void insertRowDFPointerVector(Node* p_parent, int p_index)
{
}

bool DF_Model::insertRowsDFPointerVector(const QModelIndex& p_parent)
{
    return true;
}

bool DF_Model::insertRowsSimplePointerVector(const QModelIndex& p_parent)
{

    return true;
}

bool DF_Model::insertRowsCompound(const QModelIndex& p_parent, int p_num_rows)
{
    Node* l_node = dynamic_cast<Node*>(nodeFromIndex(p_parent));
    if (l_node->m_children.size() > 0)
        return false;

    beginInsertRows(p_parent, 0, p_num_rows);
    fill_node(l_node->m_address, l_node);
    endInsertRows();
    return true;
}


bool DF_Model::has_children_from_type( NodeBase* p_node) const
{
    if (p_node->is_root_node())
        return true;

    switch (p_node->m_rdf_type)
    {
        case rdf::RDF_Type::None:
            return false;
        case rdf::RDF_Type::int64_t:
            return false;
        case rdf::RDF_Type::uint64_t:
            return false;
        case rdf::RDF_Type::int32_t:
            return false;
        case rdf::RDF_Type::int16_t:
            return false;
        case rdf::RDF_Type::uint32_t:
            return false;
        case rdf::RDF_Type::uint16_t:
            return false;
        case rdf::RDF_Type::uint8_t:
            return false;
        case rdf::RDF_Type::int8_t:
            return false;
        case rdf::RDF_Type::Void:
            return false;
        case rdf::RDF_Type::Char:
            return false;
        case rdf::RDF_Type::Long:
            return false;
        case rdf::RDF_Type::Bool:
            return false;
        case rdf::RDF_Type::Stl_string:
            return false;
        case rdf::RDF_Type::Static_string:
            return false;
        case rdf::RDF_Type::Ptr_string:
            return false;
        case rdf::RDF_Type::S_float:
            return false;
        case rdf::RDF_Type::D_float:
            return false;
        case rdf::RDF_Type::S_double:
            return false;
        case rdf::RDF_Type::Pointer:
            break;
        case rdf::RDF_Type::Array:
            return true;
        case rdf::RDF_Type::Vector:
            break;
        case rdf::RDF_Type::Bitfield:
            return true;
        case rdf::RDF_Type::Enum:
            return false;
        case rdf::RDF_Type::Compound:
            return true;
        case rdf::RDF_Type::Struct:
            return true;
        case rdf::RDF_Type::AnonymousCompound:
            return true;
        case rdf::RDF_Type::Class:
            return true;
        case rdf::RDF_Type::Union:
            return true;
        case rdf::RDF_Type::AnonymousUnion:
            return true;
//        case rdf::RDF_Type::DfArray:
//            return true;
//        case rdf::RDF_Type::Global:
//            return true;
    }

    if (p_node->m_rdf_type == rdf::RDF_Type::Pointer)
    {
        NodePointer* l_pointer = dynamic_cast<NodePointer*>(p_node);
        if (l_pointer->m_df_type == rdf::DF_Type::Void)
            return false;
        if (l_pointer->m_address == 0)
            return false;
        auto address = reinterpret_cast<uint64_t*>(l_pointer->m_address);
        if (*address != 0)
            return true;
        return false;
    }

    if (p_node->m_rdf_type == rdf::RDF_Type::Vector)
    {
        NodeVector* l_node = dynamic_cast<NodeVector*>(p_node);
        if (get_vector_size(l_node) > 0)
            return true;
        if ((l_node->m_children.size() > 1) && (l_node->m_children[0]->m_node_type != NodeType::NodeDummy))
            return true;
    }
    return false;
}

bool DF_Model::hasChildren(const QModelIndex& p_parent) const
{
    NodeBase* l_node = nodeFromIndex(p_parent);
//    if (l_node = m_rootNode)
//    {
//        return true;
//    }
    return has_children_from_type(l_node);
}




//NodeBase* locate_aux(rdf::RDFBase* p_object, Node* p_node)
//{
//    // Simple objects can't be opened in new windows
//    if (is_simple_type(p_node))
//        return nullptr;
//    if (is_string(p_node))
//        return nullptr;

//    // Check if the node is the one
//    if (p_node->m_rdf_object == p_object)
//        return p_node;

//    // If not, check its children
//    QListIterator<NodeBase*> l_iterator(p_node->m_children);
//    while (l_iterator.hasNext())
//    {
//        auto& l_child = l_iterator.next();
//        if (is_simple_type(l_child))
//            continue;
//        if (is_string(l_child))
//            continue;
//        NodeBase* l_result = locate_aux(p_object, dynamic_cast<Node*>(l_child));
//        if (l_result != nullptr)
//            return l_result;
//    }
//    // Not found in the children
//return nullptr;
//}


//NodeBase* DF_Model::locate(rdf::RDFBase* p_object)
//{
//    return locate_aux(p_object, m_rootNode);
//}



void DF_Model::insert_child_nodes(NodeBase* p_node, const QModelIndex& p_index)
{
    switch (p_node->m_rdf_type)
    {
//        case rdf::RDF_Type::DFPointer:
//            insertRowsDFPointer(p_index, 5);
//            break;
        case rdf::RDF_Type::Vector:
            insertRowsVector(p_index);
            break;
        case rdf::RDF_Type::Pointer:
            insertRowsPointer(p_index);
            break;
//        case rdf::RDF_Type::SimplePointerVector:
//            insertRowsSimplePointerVector(p_index);
//            break;
        case rdf::RDF_Type::Array:
            insertRowsArray(p_index);
            break;
        case rdf::RDF_Type::Class:
        case rdf::RDF_Type::Struct:
        case rdf::RDF_Type::Compound:
            insertRowsCompound(p_index, 5);
            break;
        case rdf::RDF_Type::Bitfield:
            insertRowsBitfield(p_index);
            break;
        default:
            break;
    }
}
